package com.guilherme.miguel;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.String.format;

@Slf4j
@SpringBootApplication
public class SpringTaskExecutor {

    public static void main(String[] args) {
        SpringApplication.run(SpringTaskExecutor.class, args);
    }

    @Bean
    public AsyncTaskExecutor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(25);

        return executor;
    }

    @Bean
    public CommandLineRunner commandLineRunner(AsyncTaskExecutor asyncExecutor) {
        return args -> {

            // Create 5 tasks
            log.info("Creating 5 tasks...");
            List<Future<String>> futures = IntStream.rangeClosed(1, 5)
                    .mapToObj(i -> asyncExecutor().submit(() -> doTask(i)))
                    .collect(Collectors.toList());

            // Wait until everything finishes
            log.info("Waiting for everything to finish...");
            List<String> list = futures.stream()
                    .map(i -> Try.of(() -> i.get()).getOrElse(""))
                    .collect(Collectors.toList());

            log.info("Done...");
            list.forEach(i -> log.info(i.toString()));
        };
    }

    /**
     * Dummy Task
     */
    private String doTask(int i) {

        log.info("Start Task {}", i);
        Try.run(() -> Thread.sleep((long) (Math.random() * 5000)));
        log.info("End Task {}", i);

        return format("Task %s", i);
    }
}


// Expected Result:

// [           main] c.g.m.SpringTaskExecutor     : Creating 5 tasks...                
// [asyncExecutor-1] c.g.m.SpringTaskExecutor     : Start Task 1                       
// [asyncExecutor-2] c.g.m.SpringTaskExecutor     : Start Task 2                       
// [asyncExecutor-3] c.g.m.SpringTaskExecutor     : Start Task 3                       
// [asyncExecutor-4] c.g.m.SpringTaskExecutor     : Start Task 4                       
// [           main] c.g.m.SpringTaskExecutor     : Waiting for everything to finish...
// [asyncExecutor-5] c.g.m.SpringTaskExecutor     : Start Task 5                       
// [asyncExecutor-4] c.g.m.SpringTaskExecutor     : End Task 4                         
// [asyncExecutor-3] c.g.m.SpringTaskExecutor     : End Task 3                         
// [asyncExecutor-1] c.g.m.SpringTaskExecutor     : End Task 1                         
// [asyncExecutor-2] c.g.m.SpringTaskExecutor     : End Task 2                         
// [asyncExecutor-5] c.g.m.SpringTaskExecutor     : End Task 5                         
// [           main] c.g.m.SpringTaskExecutor     : Done...                            
// [           main] c.g.m.SpringTaskExecutor     : Task 1                             
// [           main] c.g.m.SpringTaskExecutor     : Task 2                             
// [           main] c.g.m.SpringTaskExecutor     : Task 3                             
// [           main] c.g.m.SpringTaskExecutor     : Task 4                             
// [           main] c.g.m.SpringTaskExecutor     : Task 5                             